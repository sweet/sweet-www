---
layout: page
title: Documentation
---


There are two main sources for documentation:

## 1. Automatically generated documentation

A Doxygen-based documentation is available at [https://sweet.gitlabpages.inria.fr/sweet-doc/](https://sweet.gitlabpages.inria.fr/sweet-doc/){:target="_blank"}. The best place to start with an overview is very likely the page on [namespaces](https://sweet.gitlabpages.inria.fr/sweet-doc/namespaces.html)

This documentation is mainly about the source code.

## 2. Within the SWEET source itself

There is plenty of documentation in the [./doc](https://gitlab.inria.fr/sweet/sweet/tree/main/doc/){:target="_blank"} folder in the SWEET repository. Please have a look at this directory first:


### SWEET

* **Continuous testing** $\rightarrow$
  [/doc/sweet/continuous_testing](https://gitlab.inria.fr/sweet/sweet/blob/main/doc/sweet/continuous_testing/gitlab.md){:target="_blank"}<br />
  Information about continuous testing

* **Software development discussions** $\rightarrow$
  [/doc/sweet/software_development_discussions](https://gitlab.inria.fr/sweet/sweet/tree/main/doc/sweet/software_development_discussions){:target="_blank"}<br />
  Various information about why SWEET looks as it is


### MULE

MULE is at the heart of SWEET - if you let it in!<br />
It cares about automatic job script generation, generation of job scripts on super computers (if platforms are written properly), it cares about postprocessing and many more things.
Check out the documentation!

[/doc/MULE/](https://gitlab.inria.fr/sweet/sweet/blob/main/doc/MULE/){:target="_blank"}


### Time integration

There's plenty of information available about the different time integration methods, see
[/doc/time_integration](https://gitlab.inria.fr/sweet/sweet/tree/main/doc/time_integration){:target="_blank"}




### PDE specific information

So far there's only information available for the shallow-water equations. Check out 
[/doc/pdes](https://gitlab.inria.fr/sweet/sweet/tree/main/doc/pdes){:target="_blank"}


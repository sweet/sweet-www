---
layout: page
title: Development
---

## Repositories

* [SWEET source code repository](https://gitlab.inria.fr/sweet/sweet)

* [SWEET CI system](https://gitlab.inria.fr/mschreib/sweet-ci-tests/-/pipelines)


## Coding conventions

We always appreciate new contributors to our development.
Please checkout our debatable [coding conventions](coding_conventions.html) to ensure that your contributions are in harmony with the existing code.

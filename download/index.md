---
layout: page
title: Download
---


There are currently no official releases of SWEET.

The current version is available via gitlab at <a href="https://gitlab.inria.fr/sweet/sweet" target="_blank">SWEET repository</a>.

You can check it out with git using

```bash
$ git clone https://gitlab.inria.fr/sweet/sweet.git
```

---
layout: page
title: Tutorials
---


This page provides an overview of tutorials with SWEET

**WARNING**: SWEET is still undergoing some changes. Therefore, some tutorials might not work. We'd be very interested in getting informed about this to fix it!


## Installation

 * [Installing required packages](01_Installation/basics.html)


## Basic steps with SWEET

 * [Command line compilation & running](10_Basic/PDE_SWESphere2D_cmd_line.html)

 * [Using graphical user interface](10_Basic/PDE_SWESphere2D_GUI.html)

 * [Postprocessing data](10_Basic/postprocessing.html)

 * [Convergence benchmark with reference job](10_Basic/PDE_SWESphere2D_convergence_with_reference_job.html)

 * [Error handling](10_Basic/error.html)


## Shacks: From program arguments to their utilization

Each program can have **different variables** which are modified by particular **program arguments**.
Hence, we need to be able to do this individually for each program which is explained right here.
Such locations to process & store program arguments are called **Shacks** in SWEET.
They can also be used to validate parameters and have other functionalities which go beyond this documentation.

 * [Direct processing of Program arguments](15_ShackDictionary/programArguments.html)
 
 * [Processing with Shack registry](15_ShackDictionary/shackDictionary.html)


## Performance related topics

 * [Plan generation: Wall-clock time benchmarks](10_Basic/PDE_SWESphere2D_wallclocktime_vs_error_with_plans.html)

## MULE

 * [(TODO) Using MULE](mule/basics.html)


## Advanced

 * [LibPFASST SWE sphere programs](50_Advanced/libpfasst_PDE_SWESphere2D.html)
 * [IMEX SDC for SWE on sphere (Galewsky)](50_Advanced/imex_sdc_PDE_SWESphere2D.md)




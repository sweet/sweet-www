---
layout: page
title: LibPFASST x SWEET Programs
---


This tutorial describes how to run the SWEET programs making use of [LibPFASST](https://github.com/libpfasst/LibPFASST).
The example uses `PDE_SWESphere2D_imex_sdc` but is equivalent to using `PDE_SWESphere2D_expl_sdc`.

For the MLSDC program `PDE_SWESphere2D_mlsdc`, additional flags are available. Those are explained in section 2.

## 1. Compilation

We start by loading the SWEET environment:

```bash
$ source ./activate.sh
```

Remove existing binaries

```bash
$ make clean
```

Compile `PDE_SWESphere2D_imex_sdc.cpp` program for the shallow-water equations on the sphere using the PFASST algorithm and an MLSDC sweeper.

```bash
$ scons --program=./src/programs/libpfasst/PDE_SWESphere2D_mlsdc.cpp
```

or

```bash
$ scons --program=programs/libpfasst/PDE_SWESphere2D_mlsdc
```


To compile in debug mode, add the flag `--mode=debug`.
For the pure explicit SDC program, replace `PDE_SWESphere2D_mlsdc` with `PDE_SWESphere_expl_sdc`.
For the IMEX program, use `PDE_SWESphere2D_imex_sdc` instead.



## 2. Running the program

Execute the program with the following parameters:

```
$ ./build/programs/libpfasst/*_release --dt=180 -t 1800 -M 128 --benchmark-name=galewsky --libpfasst-nodes-type=SDC_GAUSS_LOBATTO --libpfasst-nnodes 3 --libpfasst-niters 5 -o 1800 -v 2
```

These are all the parameters you usually have to touch when running one of these programs.

Note: `timestepping-method` and `timestepping-order` do not affect the LibPFASST program! 
Use the `libpfasst-*` parameters to adjust the convergence order as needed. (see below)

* Meaning of the parameters:

    * ```dt=100 -t 1800```

		time step size of 100 seconds, run for 1800 simulation seconds

    * ```-M 128```

		Spectral resolution of 128

    * ```--benchmark-name=galewsky```

		Choose Galewsky benchmark

    * ```--libpfasst-nodes-type=SDC_GAUSS_LOBATTO --libpfasst-nnodes 3 --libpfasst-niters 5```

		Use three Gauss-Lobatto nodes per time step and do five iterations per time step.
        This yields a fourth order method (min(2·3 - 2, 5) = 4).
        Alternatively, using SDC_GAUSS_LEGENDRE: min(2·(3 - 2), 5) = 2.
        See [the documentation](https://gitlab.inria.fr/sweet/sweet/tree/main/doc/libpfasst) for an explanation.

    * ```-o 1800```

        Do output every 1800 simulation seconds, i.e., only at the end.

    * ```-v 2```

		Be a little bit more verbose

### Additional flags for MLSDC

* Meaning of the parameters:

    * ```--libpfasst-nlevels```

		The number of levels. Currently, 1 or 2 levels are supported. If you plan to use one level, consider using the SDC programs.

    * ```--libpfasst-nsweeps-coarse```

		Number of sweeps on the coarsest level.

    * ```--libpfasst-coarsening-multiplier```

		This is used to compute the amount of modes on the coarser level(s), by repeatedly applying the factor to `-M`

    * ```--libpfasst-u2```, ```--libpfasst-u4```, ```--libpfasst-u6```, ```--libpfasst-u8```

		Set artificial viscosity (hyperviscosity) of order 2, 4, 6, 8. When supplying one value, it is used for all levels. Otherwise, supply as many values
        as there are levels, separated by a comma. E.g., `--libpfasst-u2 0.3 --libpfasst-u4 0.1,0.05` on two levels.

    * ```--libpfasst-u-fields```

		Which fields to apply artificial viscosity on: can be `all`, `none`, or a (comma-separated) combination of `phi_pert`, `div`, `vrt`.


**Note:** The flags `-u` and `-U` must not be used for setting viscosity of the MLSDC program! 


## 3. Using the output

* The programs generate a bunch of .sweet binary output files:

    * ```output_prog_vrt_*.sweet```

        Prognostic vorticity field

    * ```output_prog_div_*.sweet```

        Prognostic divergence field
        
    * ```output_prog_phi_pert_*.sweet```

        Prognostic phi perturbation field

You can work with the files using the existing Python scripts, e.g., for plotting

```bash
$ mule_local/bin/mule.postprocessing.plot.sphere2d.physical file1.sweet output.png
```

For more information about all of this, see the [documentation](https://gitlab.inria.fr/sweet/sweet/tree/main/doc/time_integration/libpfasst)
